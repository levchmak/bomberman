//
// Created by User win 10 on 08.05.2022.
//

#ifndef BOMBERMAN_APP_H
#define BOMBERMAN_APP_H

class App
{
public:
	App () = default;
	~App () noexcept = default;
	void runProgramm ();
	void exitProgramm ();
	bool works ();



};

#endif //BOMBERMAN_APP_H
